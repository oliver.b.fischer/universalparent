# The Universal Parent POM

This is the parent POM for all [Apache Maven](http://maven.apache.org/) based projects of [SWE Blog](http://www.swe-blog.net).

## Homepage of the Universal Parent

The project homepage is located at [http://sweblog.bitbucket.org/](http://sweblog.bitbucket.org/). 
There you will find all generated Maven Sites for 
released versions of the Universal Parent.

## Development notes

The generated Maven site of the Universal Parent POM contains
instructions how to make changes to the Universal Parent POM.

To generate the site run `./bin/make-site` and then open
your browser at [http://localhost:8080](http://localhost:8080). 

